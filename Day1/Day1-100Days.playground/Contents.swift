/*==== Variables and Constants ====*/
// Create a variable called aVariable that contains the string value Hello
var aVariable = "Hello" 
// Changed the contents of the aVariable container
aVariable = "Hello World"

// A constant is declared with the keyword let
let aConstant = "Haters gunna hate!"

/*==== String ====*/
var aString = "Hello World! 🖖"

/*==== Int ====*/
var anInt = 1
var aLargeInt = 1000000
var alsoALargeInt = 1_000_000

/*==== Double ====*/
var aDouble = 3.141592

/*==== Bool ====*/
var aBoolean = false
aBoolean = true

/*==== Type Inference and Annotations ====*/
var anotherString = "I would like some pie"
//var aString = 3.141592 // This just won't work!

var aVariableOfTypeString: String = "Swift is awesome!"
let aConstantOfTypeDouble: Double = 3.141592
let aConstantOfTypeInteger: Int = 255

let anUnsignedInteger: UInt8 = 255
let anUnsignedIntegerInferredAsAnInt = 255


